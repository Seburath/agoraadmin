#!/usr/bin/env python
# -*- coding: utf-8 -*-

from .msgman import MsgMan


class Bot(MsgMan):
    """Bot commands management."""

    def start(self, update, context):
        """Register an User (The user need to have an @username)"""
        self.set_update(update)
        self.reply("Reputation file created for @" + self.get_user())

    def reputation(self, update, context):
        """Display User reputation"""
        self.set_update(update)
        text = update.message.text

        if len(text.split(" ")) == 1:
            self.reply("You need to give an username:\n/reputation @username")

        else:
            user = text.split(" ")[1]
            self.reply("Reputation of " + user + ":")

    def contract(self, update, context):
        """Start a contract with an User (The user needs to be registered)"""
        self.set_update(update)
        text = update.message.text


        if len(text.split(" ")) == 1:
            self.reply("You need to give an username:\n/contract @username")

        else:
            user = text.split(" ")[1]
            self.reply("Reputation of " + user + ":")

    def accept(self, update, context):
        """Accept Contract"""
        pass

    def finish(self, update, context):
        """Finish Contract"""
        pass

    def handle_text(self, update, context):
        pass

    def handle_button(self, update, context):
        pass

    def log_errors(self, update, context):
        self.log('Update "%s" caused error "%s"', update, context.error)
