#!/usr/bin/env python
# -*- coding: utf-8 -*-

from telegram.ext import (CommandHandler,
    MessageHandler,
    Filters,
    CallbackQueryHandler,
)

from .bot import Bot


if __name__ == "__main__":
    """Start the bot."""

    bot = Bot()
    dp = bot.updater.dispatcher

    dp.add_handler(CommandHandler("start", bot.start))
    dp.add_handler(CommandHandler("reputation", bot.reputation))
    dp.add_handler(CommandHandler("contract", bot.contract))
    dp.add_handler(CommandHandler("accept", bot.contract))
    dp.add_handler(CommandHandler("finish", bot.contract))

    dp.add_handler(CallbackQueryHandler(bot.handle_button))
    dp.add_handler(MessageHandler(Filters.text, bot.handle_text))

    dp.add_error_handler(bot.log_errors)

    bot.updater.start_polling()
    bot.updater.idle()


