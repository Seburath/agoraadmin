#!/usr/bin/env python3
#

import os
from setuptools import setup


def read(fname):
    """Read a file and return the content"""
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="agoraadmin",
    version="0.0.1",
    author="Seburath",
    author_email="t.me/seburath",
    description=("AgoraAgmin on Telegram"),
    license="MIT",
    keywords="Agora",
    url="http://gitlab.com/seburath/agoraadmin",
    packages=["agoraadmin"],
    long_description=read("README.md"),
    classifiers=[
        "Development Status :: 3 - Alpha",
        "Topic :: Management",
        "License :: OSI Approved :: MIT License",
    ],
)
